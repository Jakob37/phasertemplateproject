/**
 * Created by Jakob on 2015-03-29.
 */

var PhaserTemplate = PhaserTemplate || {};

PhaserTemplate.game = new Phaser.Game(800, 600, Phaser.AUTO, '');

PhaserTemplate.game.state.add('boot', PhaserTemplate.boot);
PhaserTemplate.game.state.add('preload', PhaserTemplate.preload);
PhaserTemplate.game.state.add('menu', PhaserTemplate.menu);
PhaserTemplate.game.state.add('play', PhaserTemplate.play);

PhaserTemplate.game.state.start('boot');

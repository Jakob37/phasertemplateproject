/**
 * Created by Jakob on 2015-03-28.
 */

var PhaserTemplate = PhaserTemplate || {};

PhaserTemplate.boot = function(){};

PhaserTemplate.boot.prototype = {
    preload: function() {
        this.load.image('preloadbar', 'assets/sprites/preloader-bar.png');
    },
    create: function() {

        this.game.stage.backgroundColor = '#000';
        this.scale.pageAlignHorizontally = true;

        this.state.start('preload');
    }
};
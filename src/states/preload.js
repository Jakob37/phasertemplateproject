var PhaserTemplate = PhaserTemplate || {};

PhaserTemplate.preload = function(){};

PhaserTemplate.preload.prototype = {
    preload: function() {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadbar');
        this.preloadBar.anchor.setTo(0.5);

        // Display preload
        this.load.setPreloadSprite(this.preloadBar);

        // Load game assets
        this.load.image('square', 'assets/sprites/white_square.png');
    },
    create: function() {
        this.state.start('menu');
    }
};